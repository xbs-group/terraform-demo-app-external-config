# Generate YAML file, Upload it to the GitLab repo

This is a simple example of the Terraform code, which:

- generates a `values-dev-infra.yaml` YAML file, based on:
   - some templates from the `templates` dir;
   - input from defined AWS resources;

- uploads the generated `values-dev-infra.yaml` YAML file to the specified GitLab repository.


## Preparation

1. Update values from the `00-input.tf` file;


## Use

> **NOTE**: Preparation block must be finished.

```
terraform init

# If the GitLab repo already exists, 
# the TF code should be aware of it in order to upload files there.
terraform import gitlab_group.my_gitlab_group xbs-group
terraform import gitlab_project.my_gitlab_project xbs-group/demo-app-external-config

terraform apply
```
