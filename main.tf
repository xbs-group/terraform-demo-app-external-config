resource "gitlab_group" "my_gitlab_group" {
  name        = local.gitlab_group_name
  path        = local.gitlab_group_name
}

resource "gitlab_project" "my_gitlab_project" {
  name                   = local.gitlab_repo_name
  namespace_id           = gitlab_group.my_gitlab_group.id
  description            = "Helm Values #2"
}

resource "gitlab_repository_file" "value_dev_infra_yaml" {
  project        = gitlab_project.my_gitlab_project.id
  file_path      = local.gitlab_file
  branch         = local.gitlab_branch
  content        = base64encode(local.file_content)
  author_email   = "terraform@example.com"
  author_name    = "Terraform"
  commit_message = "updated values-dev-infra.yaml"
  // file will be overwritten if it already exists and added to state
  overwrite_on_create = true

}
