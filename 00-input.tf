locals {
    gitlab_token = "<UPDATE_ME>"
    gitlab_group_name = "xbs-group"
    gitlab_repo_name = "demo-app-external-config"
    gitlab_file = "values-dev-infra.yaml" # relative to the root of the repo without a leading slash / or ./.
    gitlab_branch = "main"

    # This value:
    #    - may be defined as output for some AWS resource, for example;
    #    - will be used as an input to the template files from the `templates` dir
    cert_arn = "arn:for:some:resource:in:aws:22"
    # cert_arn = module.acm.arn

    key_1 = "sound"
    value_1 = "on"
    key_2 = "color"
    value_2 = "off"


    # https://developer.hashicorp.com/terraform/language/functions/yamlencode
    file_content = yamlencode({
        "image": {
            "annotations": {
                "project": "home",
                "test_argocd_source_2": local.cert_arn,
                "os": "linux",
                "arch": "amd64",
                "backup_enabled": "true"
                }
            },
        "movie": {
            "${local.key_1}": local.value_1,
            "${local.key_2}": local.value_2,
            },
        })

}
