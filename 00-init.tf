terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }

  backend "local" {
    path = "terraform.tfstate"
  }
}

provider "gitlab" {
  token = local.gitlab_token
}
